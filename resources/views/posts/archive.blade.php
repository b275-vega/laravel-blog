@extends('layouts.app')

@section('content')
    @foreach($posts as $post)
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">{{$post->title}}</h2>
                <p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
                <p class="card-subtitle text-muted">Created at: {{$post->user->created_at}}</p>
                <p class="card-text"> {{$post->content}}</p>
            </div>
        </div>
    @endforeach
@endsection
